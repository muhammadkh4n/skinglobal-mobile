var cSpeed=9;var cWidth=128;var cHeight=128;var cTotalFrames=18;var cFrameWidth=128;var cImageTimeout=false;var cIndex=0;var cXpos=0;var cPreloaderTimeout=false;var SECONDS_BETWEEN_FRAMES=0;function startAnimation(){document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';document.getElementById('loaderImage').style.width=cWidth+'px';document.getElementById('loaderImage').style.height=cHeight+'px';FPS=Math.round(100 / cSpeed);SECONDS_BETWEEN_FRAMES=1 / FPS;cPreloaderTimeout=setTimeout('continueAnimation()',SECONDS_BETWEEN_FRAMES / 1000);}
function continueAnimation(){cXpos+=cFrameWidth;cIndex+=1;if(cIndex>=cTotalFrames){cXpos=0;cIndex=0;}
if(document.getElementById('loaderImage'))
document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';cPreloaderTimeout=setTimeout('continueAnimation()',SECONDS_BETWEEN_FRAMES*1000);}
function stopAnimation(){clearTimeout(cPreloaderTimeout);cPreloaderTimeout=false;}
function imageLoader(s,fun)
{clearTimeout(cImageTimeout);cImageTimeout=0;genImage=new Image();genImage.onload=function(){cImageTimeout=setTimeout(fun,0)};genImage.onerror=new Function('alert(\'Could not load the image\')');genImage.src=s;}
function openNewWindow(page_url,type,window_name,width,height,top,left,features){if(!type){type='popup';}
if(!width){width=480;}
if(!height){height=480;}
if(!top){top=50;}
if(!left){left=50;}
if(!features){features='resizable,scrollbars';}
if(type=='popup'){var settings='height='+height+',';settings+='width='+width+',';settings+='top='+top+',';settings+='left='+left+',';settings+=features;win=window.open(page_url,window_name,settings);win.window.focus();}else if(type=='modal'){var html='';html+='<div id="app_common_modal">';html+='<div class="app_modal_body"><a href="javascript:void(0);" id="app_common_modal_close">X</a><iframe src="'+page_url+'" frameborder="0"></iframe></div>';html+='</div>';if(!$('#app_common_modal').length){$('body').append(html);}
$('#app_common_modal').fadeIn();}}
function queryString(){var query_string={};var query=window.location.search.substring(1);var vars=query.split("&");for(var i=0;i<vars.length;i++){var pair=vars[i].split("=");if(typeof query_string[pair[0]]==="undefined"){query_string[pair[0]]=decodeURIComponent(pair[1]);}else if(typeof query_string[pair[0]]==="string"){var arr=[query_string[pair[0]],decodeURIComponent(pair[1])];query_string[pair[0]]=arr;}else{query_string[pair[0]].push(decodeURIComponent(pair[1]));}}
return query_string;}
function asyncProspect(){$.ajax({url:'ajax.php?method=async_prospect',method:'post',data:{},success:function(data){if(typeof data=='object'&&typeof data.context!=='undefined'&&data.context.errorFound==0&&data.context.prospectId){if($('input[name=prospectId]').length){$('input[name=prospectId]').val(data.context.prospectId);}}else{$('form[name=checkout_form]').append('<input type="hidden" name="altered_action" value="new_order" />');}}});}