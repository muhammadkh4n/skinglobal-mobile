import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { BannerComponent } from './components/home/banner/banner.component';
import { ShippingFormComponent } from './components/shipping/shipping-form/shipping-form.component';
import { BottomComponent } from './components/home/bottom/bottom.component';
import { ScienceComponent } from './components/home/science/science.component';
import { BenefitsComponent } from './components/home/benefits/benefits.component';
import { SpotlightComponent } from './components/home/spotlight/spotlight.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { ContactComponent } from './components/home/contact/contact.component';
import { TermsComponent } from './components/home/terms/terms.component';
import { PrivacyComponent } from './components/home/privacy/privacy.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { PayButtonComponent } from './components/pay-button/pay-button.component';

import { HomeComponent } from './components/home/home.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CheckoutFormComponent } from './components/checkout/checkout-form/checkout-form.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { Upsell1Component } from './components/upsell1/upsell1.component';

// Services
import { StatesService } from './services/states.service';
import { AsyncService } from './services/async.service';
import { PageService } from './services/page.service';

// Routes
import { AppRoutes } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    ShippingFormComponent,
    BottomComponent,
    ScienceComponent,
    BenefitsComponent,
    SpotlightComponent,
    FooterComponent,
    ContactComponent,
    TermsComponent,
    PrivacyComponent,
    HomeComponent,
    CheckoutComponent,
    CheckoutFormComponent,
    ShippingComponent,
    PayButtonComponent,
    ThankyouComponent,
    Upsell1Component
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutes,
    FormsModule
  ],
  providers: [
    Title,
    StatesService,
    AsyncService,
    PageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
