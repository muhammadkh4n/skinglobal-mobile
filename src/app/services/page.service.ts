import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable()
export class PageService {

  constructor(
    private $http: Http
  ) { }

  // Gets the static page
  getPage(pageName: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'text/html');

    return this.$http.get('/assets/html/'+pageName, {headers:headers})
      .map(res => res);
  }

}
