import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Config } from '../config/app.config';

@Injectable()
export class AsyncService {

  constructor(private $http: Http) { }

  // load page
  getPage(pageType: string, method: string) {
    let body = 'pageType='+pageType+'&method='+method;
    let headers = new Headers();
    headers.append('Content-type', 'application/x-www-form-urlencoded');
    return this.$http.post(Config.API_URL, body, {headers: headers}).map(res => res.json());
  }

  // order trial
  orderTrial(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let body  = `firstName=${data.firstName}&`;
        body += `lastName=${data.lastName}&`;
        body += `shipAddress1=${data.shipAddress1}&`;
        body += `shipPostalCode=${data.shipPostalCode}&`;
        body += `shipCity=${data.shipCity}&`;
        body += `shipState=${data.shipState}&`;
        body += `shipCountry=${data.shipCountry}&`;
        body += `phoneNumber=${data.phoneNumber}&`;
        body += `emailAddress=${data.emailAddress}&`;
        body += `method=${data.method}`;

    return this.$http.post(
      Config.API_URL,
      body,
      {headers: headers}
    ).map(res => res.json());
  }

}
