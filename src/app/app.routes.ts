import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { Upsell1Component } from './components/upsell1/upsell1.component';

export const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'shipping', component: ShippingComponent},
  {path: 'checkout', component: CheckoutComponent, data: {discount:true}},
  {path: 'thankyou', component: ThankyouComponent},
  {path: 'upsell1', component: Upsell1Component},
]

export const AppRoutes = RouterModule.forRoot(routes);