import { Component, OnInit } from '@angular/core';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-upsell1',
  templateUrl: './upsell1.component.html',
  styleUrls: ['./upsell1.component.css']
})
export class Upsell1Component implements OnInit {

  global: Object;
  upsell: any;

  constructor() { }

  ngOnInit() {
    this.global = Global;
    this.upsell = {};
  }

}
