import { Component, OnInit } from '@angular/core';

import { AsyncService } from '../../services/async.service';
import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  global: Object;

  constructor(private Async: AsyncService) { }

  ngOnInit() {
    this.global = Global;
    this.Async.getPage('leadPage', 'importClick')
      .subscribe((res) => {
        console.log(res);
      });
  }

}
