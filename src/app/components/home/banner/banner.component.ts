import { Component, OnInit } from '@angular/core';

import * as Global from '../../../global_vars/main'; // Global vars

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
