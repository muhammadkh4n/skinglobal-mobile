import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StatesService } from '../../../services/states.service';
import { AsyncService } from '../../../services/async.service';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.css']
})
export class CheckoutFormComponent implements OnInit {

  billShipSame: number;
  statesAbbr: Array<string>;
  states: Object;
  customer: any;

  constructor(
    private State: StatesService,
    private Async: AsyncService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.State.getStates()
    .subscribe(states => {
      this.states = states;
      this.statesAbbr = Object.keys(this.states);
    });
    let savedForm = JSON.parse(localStorage.getItem('trial'));
    if (savedForm) {
      this.customer = JSON.parse(localStorage.getItem('trial')).body;
    } else {
      this.customer = {};
    }
    this.customer.billShipSame = 1;
    this.customer.discount = this.route.snapshot.data['discount'];
  }

}
