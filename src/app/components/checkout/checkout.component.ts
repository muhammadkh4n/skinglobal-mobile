import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  global: Object;
  customer: any;
  discount: any;

  constructor(
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.global = Global;
  }

}
