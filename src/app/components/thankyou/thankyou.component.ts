import { Component, OnInit } from '@angular/core';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {

  global: Object;
  customer: any;
  currency: any;

  constructor() { }

  ngOnInit() {
    this.global = Global;
    this.customer = {};
    this.currency = {};
  }

}
