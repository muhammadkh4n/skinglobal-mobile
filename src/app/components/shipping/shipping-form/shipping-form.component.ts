import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StatesService } from '../../../services/states.service';
import { AsyncService } from '../../../services/async.service';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit {

  statesAbbr: Array<string>;
  states: Object;
  trial: any;

  constructor(private State: StatesService,
              private Async: AsyncService,
              private $router: Router) {}

  ngOnInit() {
    this.State.getStates()
      .subscribe(states => {
        this.states = states;
        this.statesAbbr = Object.keys(this.states);
      });
    let savedForm = JSON.parse(localStorage.getItem('trial'));
    if (savedForm) {
      this.trial = JSON.parse(localStorage.getItem('trial')).body;
    } else {
      this.trial = {};
    }
    this.trial.shipCountry = 'US';
    this.trial.method = 'importLead';
  }

  // Post form data to Async service
  postForm() {
    this.Async.orderTrial(this.trial)
      .subscribe(res => {
        localStorage.setItem('trial', JSON.stringify(res));
        this.$router.navigateByUrl('/checkout');
      });
  }

}
