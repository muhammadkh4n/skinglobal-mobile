import { Component, OnInit } from '@angular/core';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.css']
})
export class ShippingComponent implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
