'use strict';

export const name = "BRAND SKIN PRODUCT"; //same as corp name minus llc ect.  IE Blossom cream
export const productName = "BRAND SKIN PRODUCT Cream";
export const upsell1Name = "BRAND SKIN PRODUCT Eye Serum";
export const upsell2Name = "";
export const upsell3Name = "!!UPSELL3NAME";
export const logo = 'logo.png';
export const product1 = 'product1.png';
export const upsell1Btl = 'product2.png';
export const upsell1Ship = 4.97;
export const upsell2Btl = 'product3.png';
export const upsell2Ship = 24.95;
export const upsell3Btl = 'product1.png';
export const upsell3Ship = 4.99;
export const upsell_2_math = upsell2Ship * 2;
export const roundit = upsell_2_math.toFixed(2);
//export const orderbottle = '<img src="orderbottle.png">';
//export const girl = '<img src="girl.png">';
//export const benefitsgraphic = '<img src="benefitsgraphic.png">';
//export const header = '<img src="header.png">';
//export const button = '<img src="button.png">';
export const price = (0.00).toFixed(2); // Price is for checkout functionality straight sales so it would be 0.00 if its a trial
export const trialPrice = 89.92; // Trialprice is what is in terms
export const checkPrice = 0.00; // Checkout price, 0 for trials duh...just to make math work on step 1
export const shipping = 4.99; //shipping is the same in terms and checkout
export const shipProtection_productPrice = 1.99;
export const totalTrial = 14;
export const minusShipTime = totalTrial - 4;
//export const discountprice = "export const 0.00";
//export const shipfee = "export const 0.00";
export const site = "genericurl.com";
export const phone = "(888) 888-8888";
export const eMagPhone = "(888) 888-8888";
export const email = "care@genericurl.com";
export const address = "Corp Address:  ";
export const corpName = "BRAND SKIN PRODUCT"; // Full corp name
export const returnaddress = "Fulfillment c/o BRAND SKIN PRODUCT<br>GENERIC LLC 123 GENERIC ST, GENERICVILLE, GN, 99999";
export const version = "v1";