import { SkinglobalDPage } from './app.po';

describe('skinglobal-d App', () => {
  let page: SkinglobalDPage;

  beforeEach(() => {
    page = new SkinglobalDPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
